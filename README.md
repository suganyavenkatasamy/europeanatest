# README #

This is the a LeastCommonMultiple API for a set of sequential numbers.

The application is hosted in http://localhost:8080/calculate to get the default Max range of 25 that will result 
in leastpositive number of a set of sequential number up to 25.

Set the Maxrange(end of largest number in a seauential set) both in aplication propeorties or through API path 
variable with URL http://localhost:8080/calculate/10

The result should like this 

{"processedTime":"0","number":2520}
{"processedTime":"0","number":360360}